//
//  DataModel.m
//  GiphySearch
//
//  Created by Nikolay Alexeyev on 25.05.15.
//  Copyright (c) 2015 Nikolay Alexeyev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataModel.h"
#import "Image.h"
#import "UIImage+animatedGIF.h"

@interface DataModel()

@property (nonatomic, copy) NSString *urlString;
@property (nonatomic, copy) NSString *query;
@property (nonatomic, copy) NSURL *currentRequestURL;
@property (nonatomic) NSInteger offset;
@property (nonatomic) NSInteger count;
@property (nonatomic, strong) NSOperationQueue *queue;

@end

@implementation DataModel

- (id)init {
	self = [super init];
	
	if (self) {
		self.urlString = @"https://api.giphy.com/v1/gifs/search?q=%@&api_key=dc6zaTOxFJmzC&offset=%d";
		self.images = [[NSMutableArray alloc] init];
		self.queue = [[NSOperationQueue alloc] init];
		self.queue.maxConcurrentOperationCount = 10;
	}
	
	return self;
}

- (void)loadImagesWithQuery:(NSString *)query {
	self.query = query;
	self.offset = 0;
	[self.queue cancelAllOperations];
	[self.images removeAllObjects];
	self.currentRequestURL = nil;
	
	[self sendRequestWithQuery:self.query offset:self.offset];
}

- (void)loadImagesNextPage {
	[self sendRequestWithQuery:self.query offset:(self.offset + self.count)];
}

- (void)sendRequestWithQuery:(NSString *)query offset:(NSInteger)offset {
	NSString *urlEncodedString = [query stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
	NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:self.urlString, urlEncodedString, offset]];
	if ([self.currentRequestURL.relativeString isEqualToString:requestURL.relativeString]) {
		return;
	}
	self.currentRequestURL = requestURL;
	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:requestURL];
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	
	[NSURLConnection sendAsynchronousRequest:request
									   queue:[NSOperationQueue currentQueue]
						   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
							   
							   [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
							   
							   if (connectionError) {
								   [self.delegate receivedError:connectionError];
							   }
							   else {
								   [self parseResponseData:data];
							   }
	}];
}

- (void)parseResponseData:(NSData *)data {
	NSError *error;
	NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
	
	if (error) {
		[self.delegate receivedError:error];
		return;
	}
	for (NSDictionary *item in responseData[@"data"]) {
		Image *image = [Image imageFromDictionary:item];
		if (![self.images containsObject:image]) {
			[self.images addObject:image];
		}
	}
	
	self.offset = [responseData[@"pagination"][@"offset"] integerValue];
	self.count = [responseData[@"pagination"][@"count"] integerValue];
	
	[self.delegate receivedData];
}

#pragma mark - Gif loading

- (UIImage *)getGifAtIndex:(NSInteger)index {
	Image *image = self.images[index];
	return image.imageItself;
}

- (void)loadGifAtIndex:(NSInteger)index {
	UIImage *img = [self getGifAtIndex:index];
	if (img == nil) {
		NSBlockOperation *operation = [[NSBlockOperation alloc] init];
		__weak NSBlockOperation *weakOperation = operation;
		[operation addExecutionBlock: ^ {
			if (weakOperation.isCancelled) {
				return;
			}
			Image *image = self.images[index];
			NSURL *imageURL = [NSURL URLWithString:image.imageURL];
			NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
			[[NSOperationQueue mainQueue] addOperationWithBlock:^{
				image.imageItself = [UIImage animatedImageWithAnimatedGIFData:imageData];
				if (!image.imageItself) {
					image.imageItself = [UIImage imageNamed:@"rsz_no_image"];
				}
				[self.delegate loadedImageAtIndex:index];
			}];
		}];
		[self.queue addOperation:operation];
	}
}

@end
