//
//  CollectionViewController.h
//  GiphySearch
//
//  Created by Nikolay Alexeyev on 25.05.15.
//  Copyright (c) 2015 Nikolay Alexeyev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHTCollectionViewWaterfallLayout.h"

@interface CollectionViewController : UIViewController <UICollectionViewDataSource, UISearchBarDelegate, CHTCollectionViewDelegateWaterfallLayout>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
