//
//  Image.h
//  GiphySearch
//
//  Created by Nikolay Alexeyev on 25.05.15.
//  Copyright (c) 2015 Nikolay Alexeyev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Image : NSObject

@property (nonatomic, copy) NSString *imageURL;
@property (nonatomic, copy) UIImage *imageItself;
@property (nonatomic, assign) NSInteger width;
@property (nonatomic, assign) NSInteger height;

+ (Image *)imageFromDictionary:(NSDictionary *)dictonary;

@end
