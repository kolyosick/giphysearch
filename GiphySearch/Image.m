//
//  Image.m
//  GiphySearch
//
//  Created by Nikolay Alexeyev on 25.05.15.
//  Copyright (c) 2015 Nikolay Alexeyev. All rights reserved.
//

#import "Image.h"

@implementation Image

+ (Image *)imageFromDictionary:(NSDictionary *)dictonary {
	
	Image *image = [Image new];
	
	image.imageURL = dictonary[@"images"][@"fixed_width_small"][@"url"];
	image.width = [dictonary[@"images"][@"fixed_width_small"][@"width"] integerValue];
	image.height = [dictonary[@"images"][@"fixed_width_small"][@"height"] integerValue];
	
	return image;
}

#pragma mark - NSObject

- (BOOL)isEqual:(id)image {
	if (image == self)
		return YES;
	if (!image || ![image isKindOfClass:[self class]])
		return NO;
	if (![self.imageURL isEqualToString:[(Image *)image imageURL]])
		return NO;
	return YES;
}

@end
