//
//  DataModel.h
//  GiphySearch
//
//  Created by Nikolay Alexeyev on 25.05.15.
//  Copyright (c) 2015 Nikolay Alexeyev. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataReceiveDelegate <NSObject>

- (void)receivedData;
- (void)receivedError:(NSError *)error;
- (void)loadedImageAtIndex:(NSInteger)index;

@end

@interface DataModel : NSObject

@property (strong, nonatomic) NSMutableArray *images;
@property (weak, nonatomic) id<DataReceiveDelegate> delegate;

- (void)loadImagesWithQuery:(NSString *)query;
- (void)loadImagesNextPage;

- (UIImage *)getGifAtIndex:(NSInteger)index;
- (void)loadGifAtIndex:(NSInteger)index;

@end
