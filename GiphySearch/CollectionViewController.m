//
//  CollectionViewController.m
//  GiphySearch
//
//  Created by Nikolay Alexeyev on 25.05.15.
//  Copyright (c) 2015 Nikolay Alexeyev. All rights reserved.
//

#import "CollectionViewController.h"
#import "CollectionViewCell.h"
#import "DataModel.h"
#import "Image.h"

@interface CollectionViewController () <DataReceiveDelegate>

@property (strong, nonatomic) DataModel *model;

@end

@implementation CollectionViewController

static NSString * const reuseIdentifier = @"gifCell";

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.model = [DataModel new];
	self.model.delegate = self;

	UITapGestureRecognizer *outsideTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(outsideTap:)];
	[self.collectionView addGestureRecognizer:outsideTapGesture];
	
	CHTCollectionViewWaterfallLayout *layout = (CHTCollectionViewWaterfallLayout *)self.collectionView.collectionViewLayout;
	layout.columnCount = 3;
	layout.minimumInteritemSpacing = 10.0;
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	Image *image = self.model.images[indexPath.item];
	CHTCollectionViewWaterfallLayout *layout = (CHTCollectionViewWaterfallLayout *)collectionViewLayout;
	CGFloat columnCount = (CGFloat)layout.columnCount;
	CGFloat itemWidth = (collectionView.frame.size.width - layout.minimumInteritemSpacing * (columnCount - 1)) / columnCount;
	CGFloat ratio = (float)image.height / (float)image.width;
	
	return CGSizeMake(itemWidth, itemWidth * ratio);
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.model.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
	Image *image = self.model.images[indexPath.item];
	UIImage *imageItself = [self.model getGifAtIndex:indexPath.item];
	
	cell.picture.image = nil;
	[cell.activityIndicator stopAnimating];
	
	if (imageItself) {
		cell.picture.image = imageItself;
		cell.pictureWidth.constant = image.width;
		cell.pictureHeight.constant = image.height;
	}
	else {
		[cell.activityIndicator startAnimating];
		[self.model loadGifAtIndex:indexPath.item];
	}
	
	if (indexPath.item == self.model.images.count - 1) {
		[self.activityIndicator startAnimating];
		[self.model loadImagesNextPage];
	}
	
    return cell;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[self hideKeyboard];
	[self.activityIndicator startAnimating];
	
	[self.model loadImagesWithQuery:searchBar.text];
	[self.collectionView reloadData];
}

#pragma mark - DataReceiveDelegate

- (void)receivedData {
	[self.collectionView reloadData];
	[self.activityIndicator stopAnimating];
}

- (void)receivedError:(NSError *)error {
	[self.activityIndicator stopAnimating];
	[self.collectionView reloadData];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
													message:error.localizedDescription
												   delegate:nil
										  cancelButtonTitle:NSLocalizedString(@"OK", nil)
										  otherButtonTitles:nil];
	[alert show];
}

- (void)loadedImageAtIndex:(NSInteger)index {
	if ([self.collectionView numberOfItemsInSection:0] <= index) {
		return;
	}
	[self.collectionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:index inSection:0]]];
}

#pragma mark - private

- (void)outsideTap:(UITapGestureRecognizer *)recognizer {
	[self hideKeyboard];
}

- (void)hideKeyboard {
	[self.view endEditing:YES];
}

@end
