//
//  CollectionViewCell.h
//  GiphySearch
//
//  Created by Nikolay Alexeyev on 25.05.15.
//  Copyright (c) 2015 Nikolay Alexeyev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *picture;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *pictureHeight;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *pictureWidth;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
